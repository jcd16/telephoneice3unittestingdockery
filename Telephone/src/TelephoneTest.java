import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * 
 */

/**
 * @author Jennifer
 *
 */
class TelephoneTest {

/**	@BeforeEach
	   public void setUp() {
	      _Telephone = new Telephone("1234567890", "0987654321", " 1234567890");
	   }

	   private Telephone _Telephone;
	**/
	@Test
	//Correct
	public void testFormatCorrect() {
	   String phoneFormat = Telephone.format("1234567890");

	   Assert.assertEquals("(123)456-7890", phoneFormat);
	}
	
	//Correct
	@Test
	public void testFormatCorrect2() {
		   String phoneFormat = Telephone.format("0987654321");

		   Assert.assertEquals("(098)765-4321", phoneFormat);
		}

	//Incorrect
	@Test
	public void testFormatWhitespace() {
		   String phoneFormat = Telephone.format(" 1234567890");

		   Assert.assertEquals("(123)456-7890", phoneFormat);
		}
}
